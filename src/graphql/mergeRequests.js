import gql from "graphql-tag";

const migrationMergeRequestQuery = gql`
  query migrationIssues {
    project(fullPath: "gitlab-org/gitlab") {
      mergeRequests(
        labels: ["a11y migration day", "FY25::Q4"],
        createdAfter: "2025-01-24"
        createdBefore: "2025-01-25"
      ) {
        nodes {
          id
          webUrl
          state
          labels {
            nodes {
              title
            }
          }
          approvedBy {
            nodes {
              id
              username
              name
              avatarUrl
            }
          }
          author {
            id
            username
            name
            avatarUrl
          }
        }
      }
    }
  }
`;

export default migrationMergeRequestQuery;
