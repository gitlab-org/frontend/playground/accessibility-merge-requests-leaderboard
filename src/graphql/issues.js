import gql from "graphql-tag";

const migrationIssuesQuery = gql`
  query migrationIssues {
    project(fullPath: "gitlab-org/gitlab") {
      issues(
        labelName: "deque-audit"
        state: closed
        closedAfter: "2025-01-24"
        assigneeWildcardId: ANY
      ) {
        edges {
          node {
            id
            webUrl
            assignees {
              edges {
                node {
                  id
                  username
                  name
                  avatarUrl
                }
              }
            }
          }
        }
      }
    }
  }
`;

export default migrationIssuesQuery;
